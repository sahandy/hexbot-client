package com.android.tum.de.hexbotclient.client;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.android.tum.de.hexbotclient.R;

import java.awt.font.TextAttribute;

/**
 * @author Sahand
 * @version 1.0.0
 * implements the fragment which initializes the connection to the server.
 * Gets the server address and a name as an identity of the client/controller
 */
public class ConnectToServerDialogFragment extends DialogFragment {

    private static final String TAG = "ConnectDialog";

    ConnectToServerDialogListener mListener;
    private EditText serverEditText, clientIdEditText;
    private String server, id;

    public ConnectToServerDialogFragment() {
        // Empty constructor required for dialog fragment
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        // create a view in order to access the buttons
        View inflatedView = inflater.inflate(R.layout.dialog_connect_to_server, null);
        serverEditText = (EditText) inflatedView.findViewById(R.id.server_address_editText);
        clientIdEditText = (EditText) inflatedView.findViewById(R.id.client_identifier_editText);

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(inflatedView)
                // add action buttons
                .setTitle("Connect to server")
                .setPositiveButton(R.string.connect, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // connect to server
                        Log.i(TAG, "entered onPositiveClicked");

                        server = serverEditText.getText().toString();
                        id = clientIdEditText.getText().toString();

                        mListener.onDialogPositiveClick(ConnectToServerDialogFragment.this);
                        Log.i(TAG, "exiting onPositiveClicked");
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //ConnectToServerDialogFragment.this.getDialog().cancel();
                        mListener.onDialogNegativeClick(ConnectToServerDialogFragment.this);
                    }
                });

        return builder.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the ConnectToServerDialogListener so we can send events to the host
            mListener = (ConnectToServerDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement ConnectToServerDialogListener");
        }
    }
    public interface ConnectToServerDialogListener {
        public void onDialogPositiveClick(ConnectToServerDialogFragment dialog);
        public void onDialogNegativeClick(ConnectToServerDialogFragment dialog);
    }

    public String getServerAddress() {
        Log.i(TAG, "entered getServerAddress");
        if(serverEditText.getText().toString() == null)
            return "invalid address";
        return serverEditText.getText().toString();
    }

    public String getCliendId() {
        if(clientIdEditText.getText().toString() == null)
            return "invalid client ID";
        return clientIdEditText.getText().toString();
    }
}
