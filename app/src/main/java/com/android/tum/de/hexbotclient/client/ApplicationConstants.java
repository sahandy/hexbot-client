package com.android.tum.de.hexbotclient.client;

/**
 * imported into Android Studio by Sahand on 2/25/2015.
 * This class shall hold all constants used throughout the whole application.
 * The main aim is to provide a single point of configuration.
 * The values here are not supposed to changed within one release.
 * @author Alexander
 *
 */
public class ApplicationConstants {

    //Variables used for the SMS protocol
    public static final String msg_ready = "ready";
    public static final String msg_off = "off";
    public static final String msg_left = "left";
    public static final String msg_right = "right";
    public static final String msg_forward = "forward";
    public static final String msg_backward = "backward";

    // Delimiter variables used to tokenize the components of the SMS protocol
    public static final String delimiter_colon = ":";
    public static final String delimiter_space = " ";
    public static final String delimiter_comma = ",";
    public static final String protocol_indicator = "robot";
    public static final String empty_string = "";

    // Limitation of maximum available parameters for the different command types
    public static final int maxNumberOfCommandParameters = 1;

    // Maximum value which can be passed as a parameter for a command type
    public static final int maxValueForParameter = 254;

    // Maximum distance that can be set for a Waypoint
    public static final float maxWayPointDistance = 100;

    // Default values
    //public static final String default_ExecDuration = "1";
    //public static final String default_LogLevel = "0";
    //public static final boolean default_keepScreenOn = true;
    public static final String default_SizeCommandChainQueue = "99";

    // Bluetooth Connection
    public static final String UUID_generic = "00001101-0000-1000-8000-00805F9B34FB";
    public static final String UUID_android_device = "fa87c0d0-afac-11de-8a39-0800200c9a66";

    /**
     *  Bluetooth command parts
     */
    // The following constants were taken from the Excel sheet provided by TAMU students
    public static final String bluetoothCommand_startSequence = "11111111";
    public static final String bluetoothCommand_auxiliary = "00000000";  // For Release 1 there will be no auxiliary values
    public static final String bluetoothCommand_leftMotor_forward = "01100100";
    public static final String bluetoothCommand_rightMotor_forward = "01100100";
    public static final String bluetoothCommand_leftMotor_backward = "11100100";
    public static final String bluetoothCommand_rightMotor_backward ="11100100";
    public static final String bluetoothCommand_leftMotor_turnLeft = "11100100";
    public static final String bluetoothCommand_rightMotor_turnLeft = "01100100";
    public static final String bluetoothCommand_leftMotor_turnRight = "01100100";
    public static final String bluetoothCommand_rightMotor_turnRight ="11100100";

    // HexTalker
    public static final String TALKER_PROTOCOL_IDENTIFIER = "hex";

    public static final String TALKER_DELIMITER = ":";
    public static final String TALKER_PARAM_DELIMITER = ",";

    public static final String TALKER_BUNDLE_KEY_TYPE = "type";
    public static final String TALKER_BUNDLE_KEY_CODE = "code";
    public static final String TALKER_BUNDLE_KEY_LATITUDE = "lat";
    public static final String TALKER_BUNDLE_KEY_LONGITUDE = "lon";
    public static final String TALKER_BUNDLE_KEY_LIVE_STAT = "livestat";
    public static final String TALKER_BUNDLE_KEY_NAV = "nav";
    public static final String TALKER_BUNDLE_KEY_AP_COURSE = "course";

    public static final String TALKER_TYPE_REQUEST = "req";
    public static final String TALKER_TYPE_RESPONSE = "res";
    public static final String TALKER_TYPE_COMMAND = "cmd";

    public static final String TALKER_CODE_LAST_LOCATION = "lastloc";
    public static final String TALKER_CODE_LIVE_LOCATION = "liveloc";
    public static final String TALKER_CODE_NAV = "nav";
    public static final String TALKER_CODE_AP_ON = "apon";
    public static final String TALKER_CODE_AP_OFF = "apoff";

    public static final String TALKER_EXTRA_LIVE_LOCATION_ON = "on";
    public static final String TALKER_EXTRA_LIVE_LOCATION_OFF = "off";
    public static final String TALKER_EXTRA_NAV_FORWARD = "fwd";
    public static final String TALKER_EXTRA_NAV_BACKWARD = "bwd";
    public static final String TALKER_EXTRA_NAV_RIGHT = "right";
    public static final String TALKER_EXTRA_NAV_LEFT = "left";

    public static final String TALKER_INVALID_TYPE = "ivt";
    public static final String TALKER_INVALID_CODE = "ivc";

    // Received responses from robot
    public static final int bluetoothResponse_success = 57;
    public static final int bluetoothResponse_error_invalidSyntax = 49;
    public static final int bluetoothResponse_error_invaludMotorCommand = 50;
    public static final int bluetoothResponse_error_invalidDuration = 51;
    public static final int bluetoothResponse_error_invalidAuxiliary = 52;
    public static final int bluetoothResponse_error_invalidStartSeq = 53;

    //Debugging purposes
    public static final boolean sendSmsResponseMessages = true;

    //Fallback setting for max duty cylce of motors
    public static final String defaultMaxDutyCycle  = "100";

    //Fallback setting for duration
    public static final String defaultCommandDuration = "2";
}
