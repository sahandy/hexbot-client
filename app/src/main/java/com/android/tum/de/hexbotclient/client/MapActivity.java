package com.android.tum.de.hexbotclient.client;

import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

import com.android.tum.de.hexbotclient.R;
import com.android.tum.de.hexbotclient.comm.HexTalker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * @author Sahand
 * @version 1.0.0
 * Composed of one MapFragment and one @ToolbarFragment
 * Request location information from the Controller
 * Set waypoint on the map and initialize the autonomous navigation procedure on the Controller
 */
public class MapActivity extends ActionBarActivity implements
        OnMapReadyCallback {

    private static final String TAG = "MapActivity";

    public static final int MAP_CODE_UPDATE_HEX_MARKER = 0;

    // Indicate if the activity is running
    static boolean active = false;

    private static GoogleMap mGoogleMap;
    private static Marker hexBotMarker;
    private static MarkerOptions hexMarkerOptions;
    private Marker startPointMarker;
    private Marker endPointMarker;
    private boolean isInWaypointMode;
    private boolean isStartPointSet;
    private boolean isEndPointSet;
    private boolean isInApMode;

    protected ToolbarFragment mToolbarFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        isInWaypointMode = false;
        isStartPointSet = false;
        isEndPointSet = false;
        isInApMode = false;

        mToolbarFragment = (ToolbarFragment)getFragmentManager().findFragmentById(R.id.toolbar_frag);
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map_frag_mapActivity);
        mapFragment.getMapAsync(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        active = true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(MainActivity.hexBotTracking)
            mToolbarFragment.btnTrackHexBot.setChecked(true);
        else
            mToolbarFragment.btnTrackHexBot.setChecked(false);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        MainActivity.logMgr.d(TAG, "Entered onStop()");

        if(hexBotMarker != null) {
            hexBotMarker.remove();
            hexBotMarker = null;
        }
        // TODO check if force finishing the activity is necessary
        active = false;
    }

//    /**
//     * Override the BACK button to force finish this activity
//     * @param keyCode
//     * @param event
//     * @return
//     */
//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event)
//    {
//        if ((keyCode == KeyEvent.KEYCODE_BACK))
//        {
//            MainActivity.logMgr.d(TAG, "Back button pressed on MapActivity");
//            finish();
//        }
//        return super.onKeyDown(keyCode, event);
//    }

    //
    // CALLBACKS
    //
    @Override
    public void onMapReady(GoogleMap map) {
        MainActivity.logMgr.i(TAG, "Entered onMapReady()");

        mGoogleMap = map;

        //UpdateUI();

        mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {

                handleOnMapClick(latLng);
            }
        });
    }
    //
    // MAP INTERACTION EVENTS
    //
    private void handleOnMapClick(LatLng latLng) {
        // Check if we're in SetWaypoint Mode
        if(isInWaypointMode) {
            // Check which step we're in right now
            if (!isStartPointSet && startPointMarker == null) { // If the start point hasn't been set...
                // ... then the click represents the start point
                MarkerOptions startPointMarkerOptions = new MarkerOptions()
                        .position(new LatLng(latLng.latitude, latLng.longitude))
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                        .title("Start Point")
                        .draggable(true)
                        .snippet("Lat: " + latLng.latitude + " | Lon: " + latLng.longitude);
                startPointMarker = mGoogleMap.addMarker(startPointMarkerOptions);

                return;
            }
            if (!isEndPointSet && isStartPointSet && endPointMarker == null) {
                // If the start point is already set, but end point is not...
                // ...
                MarkerOptions endPointMarkerOptions = new MarkerOptions()
                        .position(new LatLng(latLng.latitude, latLng.longitude))
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
                        .title("End Point")
                        .draggable(true)
                        .snippet("Lat: " + latLng.latitude + " | Lon: " + latLng.longitude);
                endPointMarker = mGoogleMap.addMarker(endPointMarkerOptions);
            }
        } else
            Toast.makeText(getApplicationContext(), "not in Set WayPoint Mode", Toast.LENGTH_SHORT).show();
    }

    //
    // MAP EXTERNAL EVENTS
    //
    public static void updateMap(int code, LatLng latLng) {
        MainActivity.logMgr.d(TAG, "entered updateMap");
        if(code == MAP_CODE_UPDATE_HEX_MARKER) {
            MainActivity.logMgr.d(TAG, "Map CODE UPDATE HEX MARKER");
            if (hexBotMarker != null) {
                MainActivity.logMgr.d(TAG, "just update the existing marker");
                hexBotMarker.setPosition(latLng);
                hexBotMarker.setSnippet("Lat: " + latLng.latitude + " | Lon: " + latLng.longitude);
                hexBotMarker.setVisible(true);
            } else {
                MainActivity.logMgr.d(TAG, "marker is null, add one!");
                hexMarkerOptions = new MarkerOptions()
                        .position(latLng)
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
                        .title("HexBot")
                        .snippet("Lat: " + latLng.latitude + " | Lon: " + latLng.longitude)
                        .draggable(false)
                        .visible(true);
                hexBotMarker = mGoogleMap.addMarker(hexMarkerOptions);
            }
//            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15), 2000, null);
        }
    }

    //
    // BUTTON EVENTS
    //
    public void getLocationButtonHandler(View view) {
        MainActivity.logMgr.i(TAG, "Clicked: Find HexBot!");

        HexTalker reqTalker = new HexTalker(getApplicationContext());
        reqTalker.setType(HexTalker.TYPE_REQUEST);
        reqTalker.setCode(HexTalker.CODE_LAST_LOCTATION);
        // last location request does not contain EXTRA
        reqTalker.formatOutMessage();

        MainActivity.mSocketHelper.sendEvent("new message", reqTalker.getOutMessage());
//        Toast.makeText(getApplicationContext(), "msg: " + reqTalker.getOutMessage(), Toast.LENGTH_SHORT).show();
    }

    public void trackHexBotButtonHandler(View view){
        MainActivity.logMgr.i(TAG, "Clicked: Track HexBot");

        HexTalker reqTalker = new HexTalker(getApplicationContext());
        // TRACK/UNTRACK
        // If we are not tracking the robot, send the request to track it
        if(mToolbarFragment.btnTrackHexBot.isChecked()) {
            MainActivity.hexBotTracking = true;

            reqTalker.setType(HexTalker.TYPE_REQUEST);
            reqTalker.setCode(HexTalker.CODE_LIVE_LOCTATION);
            Bundle b = new Bundle();
            b.putString(
                    ApplicationConstants.TALKER_BUNDLE_KEY_LIVE_STAT,
                    ApplicationConstants.TALKER_EXTRA_LIVE_LOCATION_ON);
            reqTalker.setExtra(HexTalker.EXTRA_TRACKING_STATE, b);
            reqTalker.formatOutMessage();

            MainActivity.mSocketHelper.sendEvent("new message", reqTalker.getOutMessage());
            mToolbarFragment.btnTrackHexBot.setChecked(true);
            // TODO Further error handling: wait for the response to see if Controller is ready to pass live location data
        } else {
            // user hit the button to Stop Tracking the robot
            MainActivity.hexBotTracking = false;
            reqTalker.setType(HexTalker.TYPE_REQUEST);
            reqTalker.setCode(HexTalker.CODE_LIVE_LOCTATION);
            Bundle b = new Bundle();
            b.putString(
                    ApplicationConstants.TALKER_BUNDLE_KEY_LIVE_STAT,
                    ApplicationConstants.TALKER_EXTRA_LIVE_LOCATION_OFF);
            reqTalker.setExtra(HexTalker.EXTRA_TRACKING_STATE, b);
            reqTalker.formatOutMessage();

            MainActivity.mSocketHelper.sendEvent("new message", reqTalker.getOutMessage());

            mToolbarFragment.btnTrackHexBot.setChecked(false);
        }
    }

    public void setWaypointButtonHandler(View view) {
        MainActivity.logMgr.i(TAG, "Clicked: Set Waypoint Button!");

        if (isInWaypointMode) {
            // HINT: user hit "Cancel Waypoint"
            //       stop setwaypoint procedure

            // remove (if any) start/end point markers
            if(startPointMarker != null) {
                startPointMarker.remove();
                startPointMarker = null;
            }
            if(endPointMarker != null){
                endPointMarker.remove();
                endPointMarker = null;
            }


            isInWaypointMode = false;
            isStartPointSet = false;
            isEndPointSet = false;

            mToolbarFragment.changeWaypointButtonText(getString(R.string.set_waypoint));
            mToolbarFragment.changeGuideWayPointText(getString(R.string.waypoint_guide_text_default));
            mToolbarFragment.btnStart.setEnabled(false);
            mToolbarFragment.btnEnd.setEnabled(false);
            return;
        }

        // if !isInWaypointMode --> user hit "Set Waypoint" or "Reset Waypoint"--> start the procedure
        mToolbarFragment.changeWaypointButtonText(getString(R.string.cancel_waypoint));
        mToolbarFragment.changeGuideWayPointText(getString(R.string.waypoint_guide_text_set_start));
        mToolbarFragment.btnStart.setEnabled(true);
        isInWaypointMode = true;
        isStartPointSet = false;
        isEndPointSet = false;
        // remove (if any) start/end point markers
        if(startPointMarker != null) {
            startPointMarker.remove();
            startPointMarker = null;
        }
        if(endPointMarker != null){
            endPointMarker.remove();
            endPointMarker = null;
        }

        // TODO put in a different method
        // send AP_OFF to controller
        isInApMode = false;
        HexTalker reqTalker = new HexTalker(getApplicationContext());
        reqTalker.setType(HexTalker.TYPE_REQUEST);
        reqTalker.setCode(HexTalker.CODE_AP_OFF);
        // No Extra needed
        reqTalker.formatOutMessage();
        MainActivity.mSocketHelper.sendEvent("new message", reqTalker.getOutMessage());
    }

    public void confirmStartPointButtonHandler(View view) {
        MainActivity.logMgr.d(TAG, "entered confirmStartPointButtonHandler");
//        Toast.makeText(this, "confirm btn", Toast.LENGTH_SHORT).show();
        if(startPointMarker == null) {
            Toast.makeText(this, "no start point detected! Set Start Point on map", Toast.LENGTH_SHORT).show();
            return;
        } else {
            isStartPointSet = true;
            // LOCK start point
            startPointMarker.setDraggable(false);

            // Update UI
            mToolbarFragment.btnStart.setEnabled(false);
            mToolbarFragment.btnEnd.setEnabled(true);
            mToolbarFragment.changeGuideWayPointText(getString(R.string.waypoint_guide_text_set_end));

            Toast.makeText(this, "start point set successfully", Toast.LENGTH_SHORT).show();
        }
    }

    public void confirmEndPointButtonHandler(View view) {
        MainActivity.logMgr.d(TAG, "entered confirmEndPointButtonHandler");
//        Toast.makeText(this, "confirm btn", Toast.LENGTH_SHORT).show();
        if(endPointMarker == null) {
            Toast.makeText(this, "no end point detected! Set End Point on map", Toast.LENGTH_SHORT).show();
            return;
        } else {
            if (validateWaypoint()) {
                isEndPointSet = true;
                isInWaypointMode = false;
                // LOCK end point
                endPointMarker.setDraggable(false);
                // Update UI
                mToolbarFragment.btnEnd.setEnabled(false);
                mToolbarFragment.changeWaypointButtonText(getString(R.string.reset_waypoint));
                mToolbarFragment.changeGuideWayPointText(getString(R.string.waypoint_guide_text_process_complete));

                Toast.makeText(this, "end point set successfully", Toast.LENGTH_SHORT).show();

                LatLng start = startPointMarker.getPosition();
                LatLng end = endPointMarker.getPosition();
                // TODO notify the route calculator
                notifyAutoPilot(MainActivity.AUTO_PILOT_ON);
                isInApMode = true;
            }
            else {
                Toast.makeText(this, "start and end points are two far away! set again!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean validateWaypoint() {
        float[] results = new float[1];
        Location.distanceBetween(
                startPointMarker.getPosition().latitude,
                startPointMarker.getPosition().longitude,
                endPointMarker.getPosition().latitude,
                endPointMarker.getPosition().longitude,
                results
        );
        Toast.makeText(getApplicationContext(), "Distance: " + Float.toString(results[0]), Toast.LENGTH_SHORT).show();
        return results[0] < ApplicationConstants.maxWayPointDistance;
    }

    private void notifyAutoPilot(int state) {
        if(state == MainActivity.AUTO_PILOT_ON) {
            StringBuilder wayPointCoor = new StringBuilder();
            wayPointCoor.append(startPointMarker.getPosition().latitude);
            wayPointCoor.append(ApplicationConstants.TALKER_PARAM_DELIMITER);
            wayPointCoor.append(startPointMarker.getPosition().longitude);
            wayPointCoor.append(ApplicationConstants.TALKER_PARAM_DELIMITER);
            wayPointCoor.append(endPointMarker.getPosition().latitude);
            wayPointCoor.append(ApplicationConstants.TALKER_PARAM_DELIMITER);
            wayPointCoor.append(endPointMarker.getPosition().longitude);

            HexTalker reqTalker = new HexTalker(getApplicationContext());
            reqTalker.setType(HexTalker.TYPE_REQUEST);
            reqTalker.setCode(HexTalker.CODE_AP_ON);
            Bundle b = new Bundle();
            b.putString(ApplicationConstants.TALKER_BUNDLE_KEY_AP_COURSE, wayPointCoor.toString());
            reqTalker.setExtra(HexTalker.EXTRA_AP_COURSE, b);
            reqTalker.formatOutMessage();

            MainActivity.mSocketHelper.sendEvent("new message", reqTalker.getOutMessage());
        }
        if(state == MainActivity.AUTO_PILOT_OFF) {
        }
    }
}
