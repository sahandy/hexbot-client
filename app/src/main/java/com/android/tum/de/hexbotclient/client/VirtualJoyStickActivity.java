package com.android.tum.de.hexbotclient.client;

import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.tum.de.hexbotclient.R;
import com.android.tum.de.hexbotclient.comm.HexTalker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * @author Sahand
 * Composed of one MapFragment and one custom fragment for handling navigation commands
 * User can track the robot while operating on it via the MapFragment
 */
public class VirtualJoyStickActivity extends ActionBarActivity implements OnMapReadyCallback{
    // Debugging
    public static final String TAG = "VirtualJoyStickActivity";

    // MAP MARKER UPDATE, REQUEST CODE
    public static final int MAP_CODE_UPDATE_HEX_MARKER = 0;

    private Button fwdBtn, bwdBtn, leftBtn, rightBtn;
    private TextView movementStateTextView;

    private String  fwdMessage, bwdMessage, leftMessage, rightMessage;

    MapFragment mMapFragment;
    protected GoogleApiClient mGoogleApiClient;
    private static GoogleMap mGoogleMap;
    protected Location mLastLocation;
    private static Marker hexBotMarker;
    private static MarkerOptions hexMarkerOptions;
    private Marker startPointMarker;
    private Marker endPointMarker;

    private boolean isSocketConnected;
    // Indicate if the activity is running
    static boolean active = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_virtual_joystick);

        /*
        JOYSTICK INITIALIZATION
         */
        // JoyStick UI Preparation
        fwdBtn = (Button)findViewById(R.id.btnForward);
        bwdBtn = (Button)findViewById(R.id.btnBackward);
        leftBtn = (Button)findViewById(R.id.btnTurnLeft);
        rightBtn = (Button)findViewById(R.id.btnTurnRight);
        // prepare Navigation commands for JoyStick buttons
        prepareNavCommands();

        /*
        MapFragment INITIALIZATION
         */
        mMapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map_frag_joystickActivity);
        mMapFragment.getMapAsync(this);

        /*
        EVENT HANDLING FOR NAVIGATION BUTTONS
         */
        // FWD Button
        fwdBtn.setOnTouchListener(fwdBtnListener);
        // BWD Button
        bwdBtn.setOnTouchListener(bwdBtnListener);
        // LEFT Button
        leftBtn.setOnTouchListener(leftBtnListener);
        // RIGHT Button
        rightBtn.setOnTouchListener(rightBtnListener);

    }

    @Override
    protected void onStart() {
        super.onStart();
        active = true;
    }
    @Override
    public void onResume(){
        super.onResume();
        MainActivity.logMgr.d(TAG, "Entered onResume");

        MainActivity.logMgr.d(TAG, "nav messages...");
        MainActivity.logMgr.d(TAG, fwdMessage);
        MainActivity.logMgr.d(TAG, bwdMessage);
        MainActivity.logMgr.d(TAG, leftMessage);
        MainActivity.logMgr.d(TAG, rightMessage);

        isSocketConnected = MainActivity.mSocketHelper.socket.connected();
    }

    @Override
    public void onStop(){
        super.onStop();

        if(hexBotMarker != null) {
            hexBotMarker.remove();
            hexBotMarker = null;
        }

        active = false;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }

    private void prepareNavCommands() {
        HexTalker fwdTalker, bwdTalker, leftTalker, rightTalker;

        fwdTalker = new HexTalker(this);
        bwdTalker = new HexTalker(this);
        leftTalker = new HexTalker(this);
        rightTalker = new HexTalker(this);

        // prepare NAV: forward message
        fwdTalker.setType(HexTalker.TYPE_COMMAND);
        fwdTalker.setCode(HexTalker.CODE_NAV);
        Bundle fwdBundle = new Bundle();
        fwdBundle.putString(
                ApplicationConstants.TALKER_BUNDLE_KEY_NAV,
                ApplicationConstants.TALKER_EXTRA_NAV_FORWARD);
        fwdTalker.setExtra(HexTalker.EXTRA_NAV, fwdBundle);
        fwdTalker.formatOutMessage();
        fwdMessage = fwdTalker.getOutMessage();

        // prepare NAV: backward message
        bwdTalker.setType(HexTalker.TYPE_COMMAND);
        bwdTalker.setCode(HexTalker.CODE_NAV);
        Bundle bwdBundle = new Bundle();
        bwdBundle.putString(
                ApplicationConstants.TALKER_BUNDLE_KEY_NAV,
                ApplicationConstants.TALKER_EXTRA_NAV_BACKWARD);
        bwdTalker.setExtra(HexTalker.EXTRA_NAV, bwdBundle);
        bwdTalker.formatOutMessage();
        bwdMessage = bwdTalker.getOutMessage();

        // prepare NAV: left message
        leftTalker.setType(HexTalker.TYPE_COMMAND);
        leftTalker.setCode(HexTalker.CODE_NAV);
        Bundle leftBundle = new Bundle();
        leftBundle.putString(
                ApplicationConstants.TALKER_BUNDLE_KEY_NAV,
                ApplicationConstants.TALKER_EXTRA_NAV_LEFT);
        leftTalker.setExtra(HexTalker.EXTRA_NAV, leftBundle);
        leftTalker.formatOutMessage();
        leftMessage = leftTalker.getOutMessage();

        // prepare NAV: right message
        rightTalker.setType(HexTalker.TYPE_COMMAND);
        rightTalker.setCode(HexTalker.CODE_NAV);
        Bundle rightBundle = new Bundle();
        rightBundle.putString(
                ApplicationConstants.TALKER_BUNDLE_KEY_NAV,
                ApplicationConstants.TALKER_EXTRA_NAV_RIGHT);
        rightTalker.setExtra(HexTalker.EXTRA_NAV, rightBundle);
        rightTalker.formatOutMessage();
        rightMessage = rightTalker.getOutMessage();
    }

    private OnTouchListener fwdBtnListener = new OnTouchListener() {
        private Handler mHandler;
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch(event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    if (mHandler != null) return true;
                    mHandler = new Handler();
                    mHandler.postDelayed(mActionFwd, 250);
                    break;
                case MotionEvent.ACTION_UP:
                    if (mHandler == null) return true;
                    mHandler.removeCallbacks(mActionFwd);
                    mHandler = null;
                    break;
            }
            return false;
        }
        Runnable mActionFwd = new Runnable() {
            @Override public void run() {
                // Check if Bluetooth devices are connected
                if(!isSocketConnected){
                    MainActivity.logMgr.e(TAG, "no connection to Controller");
                }
                else{
                    // Send the navigation message: FORWARD
                    MainActivity.mSocketHelper.sendEvent("new message", fwdMessage);
//                        Toast.makeText(getApplicationContext(), "emitting FWD", Toast.LENGTH_SHORT).show();
                }
                mHandler.postDelayed(this, 2000);
            }
        };
    };

    private OnTouchListener bwdBtnListener = new OnTouchListener() {
        private Handler mHandler;
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch(event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    if (mHandler != null) return true;
                    mHandler = new Handler();
                    mHandler.postDelayed(mActionBwd, 250);
                    break;
                case MotionEvent.ACTION_UP:
                    if (mHandler == null) return true;
                    mHandler.removeCallbacks(mActionBwd);
                    mHandler = null;
                    break;
            }
            return false;
        }
        Runnable mActionBwd = new Runnable() {
            @Override public void run() {
                // Check if Bluetooth devices are connected
                if(!isSocketConnected){
                    MainActivity.logMgr.e(TAG, "no connection to Controller");
                }
                else{
                    // Send the navigation message: LEFT
                    MainActivity.mSocketHelper.sendEvent("new message", bwdMessage);
//                        Toast.makeText(getApplicationContext(), "emitting FWD", Toast.LENGTH_SHORT).show();
                }
                mHandler.postDelayed(this, 2000);
            }
        };
    };

    private OnTouchListener leftBtnListener = new OnTouchListener() {
        private Handler mHandler;
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch(event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    if (mHandler != null) return true;
                    mHandler = new Handler();
                    mHandler.postDelayed(mActionLeft, 250);
                    break;
                case MotionEvent.ACTION_UP:
                    if (mHandler == null) return true;
                    mHandler.removeCallbacks(mActionLeft);
                    mHandler = null;
                    break;
            }
            return false;
        }
        Runnable mActionLeft = new Runnable() {
            @Override public void run() {
                // Check if Bluetooth devices are connected
                if(!isSocketConnected){
                    MainActivity.logMgr.e(TAG, "no connection to Controller");
                }
                else{
                    // Send the navigation message: LEFT
                    MainActivity.mSocketHelper.sendEvent("new message", leftMessage);
//                        Toast.makeText(getApplicationContext(), "emitting FWD", Toast.LENGTH_SHORT).show();
                }
                mHandler.postDelayed(this, 2000);
            }
        };
    };

    private OnTouchListener rightBtnListener = new OnTouchListener() {
        private Handler mHandler;
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch(event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    if (mHandler != null) return true;
                    mHandler = new Handler();
                    mHandler.postDelayed(mActionRight, 250);
                    break;
                case MotionEvent.ACTION_UP:
                    if (mHandler == null) return true;
                    mHandler.removeCallbacks(mActionRight);
                    mHandler = null;
                    break;
            }
            return false;
        }
        Runnable mActionRight = new Runnable() {
            @Override public void run() {
                // Check if Bluetooth devices are connected
                if(!isSocketConnected){
                    MainActivity.logMgr.e(TAG, "no connection to Controller");
                }
                else{
                    // Send the navigation message: RIGHT
                    MainActivity.mSocketHelper.sendEvent("new message", rightMessage);
//                        Toast.makeText(getApplicationContext(), "emitting FWD", Toast.LENGTH_SHORT).show();
                }
                mHandler.postDelayed(this, 2000);
            }
        };
    };

    /**
     * MapFragment events and callbacks
     */
    @Override
    public void onMapReady(GoogleMap map) {
        MainActivity.logMgr.i(TAG, "Entered onMapReady()");
        mGoogleMap = map;
    }

    //
    // MAP EXTERNAL EVENTS
    //
    protected static void updateMap(int code, LatLng latLng) {
        MainActivity.logMgr.d(TAG, "entered updateMap");
        if(code == MAP_CODE_UPDATE_HEX_MARKER) {
            MainActivity.logMgr.d(TAG, "Map CODE UPDATE HEX MARKER");
            if (hexBotMarker != null) {
                MainActivity.logMgr.d(TAG, "just update the existing marker");
                hexBotMarker.setPosition(latLng);
                hexBotMarker.setSnippet("Lat: " + latLng.latitude + " | Lon: " + latLng.longitude);
                hexBotMarker.setVisible(true);
            } else {
                MainActivity.logMgr.d(TAG, "marker is null, add one!");
                hexMarkerOptions = new MarkerOptions()
                        .position(latLng)
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
                        .title("HexBot")
                        .snippet("Lat: " + latLng.latitude + " | Lon: " + latLng.longitude)
                        .draggable(false)
                        .visible(true);
                hexBotMarker = mGoogleMap.addMarker(hexMarkerOptions);
            }
//            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15), 2000, null);
        }
    }
}
