package com.android.tum.de.hexbotclient.client;

import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.tum.de.hexbotclient.comm.HexTalker;
import com.android.tum.de.hexbotclient.log.*;

import com.android.tum.de.hexbotclient.R;
import com.android.tum.de.hexbotclient.util.HexLocation;
import com.github.nkzawa.emitter.Emitter;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;


public class MainActivity extends ActionBarActivity implements
        ConnectToServerDialogFragment.ConnectToServerDialogListener{

    // Debugging
    public static final String TAG = "MainActivity";

    /*
    * INTENT REQUEST CODES
    */
    private static final int REQUEST_MANUAL_NAVIGATION = 3;

    /*
    * HEX TALKER CODES
    */
    public static final int HEX_MESSAGE_REQUEST = 0;
    public static final int HEX_MESSAGE_RESPONSE = 1;

    /*
    * AutoPilot CODES
    */
    public static final int AUTO_PILOT_OFF = 0;
    public static final int AUTO_PILOT_ON = 1;

    /*
    * Global flags
    */
    public static boolean hexBotTracking;


    // HexLocation
    protected HexLocation mHexLocation;

    // UI elements
    private Button btnConnect, btnDisconnect;
    private TextView txtConnectionState;

    /* For Testing purpose, checks whether the App is capable of sending a string to the other device
	private Button testSendBtn;
	private EditText txtOut;*/
    private static TextView mLogTextView;

    SharedPreferences sharedPrefs;

    // ConnectToServer DialogFragment
    ConnectToServerDialogFragment mConnectDialog;
    public static SocketHelper mSocketHelper;

    // Handler
    Handler hexHandler = new HexHandler( new WeakReference<MainActivity>(this) );


    // Custom Log functionality
    public static CustomLogManager logMgr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /**
         * Initialization
         */
        // Initialization of preferences values for the case when user enters the application
        // FOR THE FIRST TIME
        PreferenceManager.setDefaultValues(getApplicationContext(), R.xml.preferences, false);

        // Get preferences values from SharedPreferences object
        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        // UI
        //Initialize the custom logging functionality
        mLogTextView = (TextView)findViewById(R.id.txtView_LogConsole);
        mLogTextView.setMovementMethod(new ScrollingMovementMethod());
        txtConnectionState = (TextView)findViewById(R.id.txtView_connection_state);
        btnConnect = (Button)findViewById(R.id.button_connect_to_controller);
        btnDisconnect = (Button)findViewById(R.id.button_disconnect_from_controller);

        btnConnect.setEnabled(true);
        btnDisconnect.setEnabled(false);

        String temp = sharedPrefs.getString( getResources().getString(R.string.pref_logLevel_key), getResources().getString(R.string.pref_logLevel_default) );
        logMgr = new CustomLogManager(LogLevel.valueOf(temp) );
        logMgr.init();

        // Button Events
        btnConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connect();
            }
        });
        btnDisconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mSocketHelper.disconnect()) {
                    Toast.makeText(getApplicationContext(), "error disconnecting", Toast.LENGTH_SHORT).show();
                    return;
                }
                Toast.makeText(getApplicationContext(), "Connection with server terminated", Toast.LENGTH_SHORT).show();
                txtConnectionState.setText(R.string.txt_connection_state_disconnected);
                txtConnectionState.setTextColor(Color.RED);

                btnConnect.setEnabled(true);
                btnDisconnect.setEnabled(false);
            }
        });

        mSocketHelper = new SocketHelper();

        mHexLocation = new HexLocation(getApplicationContext());
        mHexLocation.init();
        hexBotTracking = false;
    }

    @Override
    public void onResume() {
        super.onResume();

        // Set KeepScreen On/Off
        boolean keepScreenOn = sharedPrefs.getBoolean(getResources().getString(R.string.pref_keep_screen_on_key), true);
        if (keepScreenOn) {
            this.getWindow().addFlags(android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        } else {
            this.getWindow().clearFlags(android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }

        mHexLocation.connectGoogleApiClient();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        logMgr.d(TAG, "Entered onStop()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        logMgr.d(TAG, "Entered onDestroy()");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.menuMap:
                if(mSocketHelper == null) {
                    Toast.makeText(this, "Not connected to server.", Toast.LENGTH_SHORT).show();
                    break;
                }
                if (!mSocketHelper.socket.connected()) {
                    Toast.makeText(this, "Not connected to server.", Toast.LENGTH_SHORT).show();
                    break;
                }
                Intent mapIntent = new Intent(MainActivity.this, MapActivity.class);
                startActivity(mapIntent);
                break;
            case R.id.menuVirtualJoyStick:
                if (mSocketHelper == null) {
                    Toast.makeText(this, "Not connected to server.", Toast.LENGTH_SHORT).show();
                    break;
                }
                if (!mSocketHelper.socket.connected()) {
                    Toast.makeText(this, "Not connected to server.", Toast.LENGTH_SHORT).show();
                    break;
                }
                Intent virtualJoyStickIntent = new Intent(MainActivity.this, VirtualJoyStickActivity.class);
                startActivity(virtualJoyStickIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Use this method to display any messages on the log text view on the main screen
     * @param message The formatted message to be displayed
     */
    public static synchronized void writeToTextView(final String message) {
        mLogTextView.post(new Runnable() {
            @Override
            public void run() {
                mLogTextView.append(message);
            }
        });
    }

    // Connect to the Controller App
    private void connect() {
//
//        logMgr.d(TAG, "Entered conenct()");
//
//        String serverAddress = "https://hex-chat-master.herokuapp.com";
//
//        if( !mSocketHelper.connect(serverAddress) ) { // If connection failed...
//            Toast.makeText(
//                    getApplicationContext(),
//                    "Invalid server address",
//                    Toast.LENGTH_SHORT
//            ).show();
//            return;
//        }
//        logMgr.d(TAG, "Connected to server successfully");
//        // Connected successfully, update the connection state text
//        txtConnectionState.setText(R.string.txt_connection_state_connected);
//        txtConnectionState.setTextColor(Color.parseColor("#669900"));
//        btnConnect.setEnabled(false);
//        btnDisconnect.setEnabled(true);
//        mSocketHelper.sendEvent("add user", "CLIENT");
//        mSocketHelper.addListener("new message",new_message);
//
        mConnectDialog = new ConnectToServerDialogFragment();
        mConnectDialog.show(getFragmentManager(), "ConnectDialogFragment");
    }

    // CALLBACKS
    /* The dialog fragment receives a reference to this Activity through the
    *  Fragment.onAttach() callback, which it uses to call the following methods
    *  defined by the NoticeDialogFragment.NoticeDialogListener interface
    */
    @Override
    public void onDialogPositiveClick(ConnectToServerDialogFragment dialog) {
        // User touched the dialog's positive button

        mSocketHelper = new SocketHelper();
        if( !mSocketHelper.connect(dialog.getServerAddress()) ) { // If connection failed...
            Toast.makeText(
                    getApplicationContext(),
                    "Invalid server address",
                    Toast.LENGTH_SHORT
            ).show();
            return;
        }
        // Connected successfully, update the connection state text
        txtConnectionState.setText(R.string.txt_connection_state_connected);
        txtConnectionState.setTextColor(Color.parseColor("#669900"));
        btnConnect.setEnabled(false);
        btnDisconnect.setEnabled(true);
        mSocketHelper.sendEvent("add user", dialog.getCliendId());
        mSocketHelper.addListener("new message",new_message);
    }

    @Override
    public void onDialogNegativeClick(ConnectToServerDialogFragment dialog) {
        // User touched the dialog's negative button
        Toast.makeText(
                getApplicationContext(),
                "Dialog: Cancelled by user",
                Toast.LENGTH_SHORT
        ).show();
    }

//    protected static void notifyAutoPilot(LatLng start, LatLng end, int state) {
//        if(state == AUTO_PILOT_ON) {
//        }
//        if(state == AUTO_PILOT_OFF) {
//        }
//    }

    Emitter.Listener new_message = new Emitter.Listener(){
        @Override
        public void call(Object... args) {
            final JSONObject data = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        logMgr.d(TAG, "emitter listener: received message!");
                        logMgr.d(TAG, data.getString("message"));
                        handleIncomingMessage(data.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        }
    };

    public void handleIncomingMessage(String msg) {
        logMgr.d(TAG, "entered handleIncomingMessage");
        HexTalker talker = new HexTalker(getApplicationContext(), hexHandler);

        talker.setInMessage(msg);
        logMgr.d(TAG, "set in message");
        talker.initializeParser();
        logMgr.d(TAG, "initialized parser");
        talker.parseIncomingMessage();
        logMgr.d(TAG, "finished parsing in message");

    }

    // Handler
    static class HexHandler extends Handler {
        WeakReference<MainActivity> mParent;

        public HexHandler(WeakReference<MainActivity> parent) {
            mParent = parent;
        }

        @Override
        public void handleMessage(Message msg) {
            MainActivity parent = mParent.get();

            logMgr.d(TAG, "entered hexHandler");
            logMgr.d(TAG, "msg.what: " + String.valueOf(msg.what));
            logMgr.d(TAG, "msg.arg1: " + String.valueOf(msg.arg1));
            switch (msg.what) {
                case HEX_MESSAGE_REQUEST:
                    // for the moment: we don't have any REQUEST coming from Controller App
                    break;
                case HEX_MESSAGE_RESPONSE:
                    switch (msg.arg1) {
                        case HexTalker.CODE_LAST_LOCTATION:
                            logMgr.d(TAG, "=== HERE 1 ===");
                            LatLng coor = (LatLng) msg.obj;
                            logMgr.d(TAG, "handler > lat: " + coor.latitude);
                            logMgr.d(TAG, "handler > lon: " + coor.longitude);
                            if(MapActivity.active) {
                                MapActivity.updateMap(
                                        MapActivity.MAP_CODE_UPDATE_HEX_MARKER,
                                        (LatLng) msg.obj);
                            }
                            if(VirtualJoyStickActivity.active) {
                                VirtualJoyStickActivity.updateMap(
                                        MapActivity.MAP_CODE_UPDATE_HEX_MARKER,
                                        (LatLng) msg.obj);
                            }
                            break;
                        case HexTalker.CODE_LIVE_LOCTATION:
                            logMgr.d(TAG, "Handler: CODE_LVE_LOCATION");
                            LatLng liveCoor = (LatLng) msg.obj;
                            logMgr.d(TAG, "handler > lat: " + liveCoor.latitude);
                            logMgr.d(TAG, "handler > lon: " + liveCoor.longitude);
                            if(MapActivity.active) {
                                MapActivity.updateMap(
                                        MapActivity.MAP_CODE_UPDATE_HEX_MARKER,
                                        (LatLng) msg.obj);
                            }
                            if(VirtualJoyStickActivity.active) {
                                VirtualJoyStickActivity.updateMap(
                                        MapActivity.MAP_CODE_UPDATE_HEX_MARKER,
                                        (LatLng) msg.obj);
                            }
                            break;
                    }
                    break;
            }
        }
    }

    // CALLBACKS

}
