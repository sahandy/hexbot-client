package com.android.tum.de.hexbotclient.client;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.android.tum.de.hexbotclient.R;

/**
 * Created by Sahand on 2/20/2015.
 */
public class ToolbarFragment extends Fragment {

    private static final String TAG = "ToolbarFragment";

    private ButtonClickListener mClickListener = null;
    private int FIND_HEXBOT_REQUEST_CODE = 0;
    private int MOCK_BUTTON_REQUEST_CODE = 1;
    Button btnLoc, btnSetWaypoint, btnStart, btnEnd;
    ToggleButton btnTrackHexBot;
    public TextView waypointGuideTextView;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.toolbar_map, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        btnLoc = (Button) getActivity().findViewById(R.id.get_location_button);
        btnSetWaypoint = (Button) getActivity().findViewById(R.id.set_waypoint_button);
        btnStart = (Button) getActivity().findViewById(R.id.confirm_start_point_button);
        btnEnd = (Button) getActivity().findViewById(R.id.confirm_end_point_button);
        btnTrackHexBot = (ToggleButton) getActivity().findViewById(R.id.track_hexbot_button);

        waypointGuideTextView = (TextView) getActivity().findViewById(R.id.waypoint_guide_textView);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();

        btnStart.setEnabled(false);
        btnEnd.setEnabled(false);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public interface ButtonClickListener {
        public void onButtonSelection(int requestCode);
    }

    public void changeWaypointButtonText(String text) {
        Log.i(TAG, "entered changeWaypointButtonText");
        btnSetWaypoint.setText(text);
    }

    public void changeGuideWayPointText(String text) {
        Log.i(TAG, "entered changeGuideWayPointText");
        waypointGuideTextView.setText(text);
    }
}
