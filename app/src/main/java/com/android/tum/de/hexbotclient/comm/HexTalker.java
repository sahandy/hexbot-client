package com.android.tum.de.hexbotclient.comm;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.android.tum.de.hexbotclient.R;
import com.android.tum.de.hexbotclient.client.ApplicationConstants;
import com.android.tum.de.hexbotclient.client.MainActivity;
import com.google.android.gms.maps.model.LatLng;

/**
 * implements the necessary message protocols for communication between Client and Controller
 * creates the outgoing messages, as well as parsing incoming ones and inform different modules
 * according to the specified aciton
 */
public class HexTalker {

    private static final String TAG = "HexTalker";

    /*
    REQUEST CODES
     */
    public static final int TYPE_REQUEST = 110;
    public static final int TYPE_RESPONSE = 120;
    public static final int TYPE_COMMAND = 130;

    public static final int CODE_LAST_LOCTATION = 210;
    public static final int CODE_LIVE_LOCTATION = 220;
    public static final int CODE_NAV = 230;
    public static final int CODE_AP_ON = 240;
    public static final int CODE_AP_OFF = 241;

    public static final int EXTRA_LOCATION = 310;
    public static final int EXTRA_TRACKING_STATE = 320;
    public static final int EXTRA_NAV = 330;
    public static final int EXTRA_AP_COURSE = 340;

    public static final int TRACKING_STATE_ON = 321;
    public static final int TRACKING_STATE_OFF = 322;
    public static final int TRACKING_DATA = 323;
    public static final int NAV_FORWARD = 331;
    public static final int NAV_BACKWARD = 332;
    public static final int NAV_LEFT = 333;
    public static final int NAV_RIGHT = 334;

    private Handler mHandler;

    private String message;
    private String inMessage;
    private StringBuilder outMessage = new StringBuilder();
    private String[] splittedMessage;
    private final Context mContext;
    private double mLatitude;
    private double mLongitude;

    private HexResponse mHexResponse;
    private HexRequest mHexRequest;

    // ============================
    private String messageType;
    private String messageCode;
    private String messageExtra;
    // ============================

    public HexTalker(Context context) {
        mContext = context;
        mHexRequest = new HexRequest();
        mHexResponse = new HexResponse();
        outMessage.append(ApplicationConstants.TALKER_PROTOCOL_IDENTIFIER);
        outMessage.append(ApplicationConstants.TALKER_DELIMITER);

        message = null;
    }

    public HexTalker(Context context, Handler handler) {
        mContext = context;
        mHandler = handler;

        mHexRequest = new HexRequest();
        mHexResponse = new HexResponse();

        messageType = "";
        messageCode = "";
        messageExtra = "";

        inMessage = null;

        outMessage.append(ApplicationConstants.TALKER_PROTOCOL_IDENTIFIER);
        outMessage.append(ApplicationConstants.TALKER_DELIMITER);
    }

    public boolean initializeParser() {
        inMessage = inMessage.trim();
        splittedMessage = inMessage.split(ApplicationConstants.TALKER_DELIMITER);
        return splittedMessage.length != 0;
    }

    public boolean parseIncomingMessage() {
        // 1. validate IDENTIFIER
        if( !validateMessageIdentifier() ) {
            MainActivity.logMgr.e(TAG, "invalid message: wrong identifier");
            return false;
        }

        // 2. validate TYPE
        if(!validateInMessageType()) {
            MainActivity.logMgr.e(TAG, "invalid message: wrong type");
            return false;
        }
        // 3. validate CODE
        // 4. get EXTRA
        if(!validateInMessageCode()) {
            MainActivity.logMgr.e(TAG, "invalid message: wrong code");
            return false;
        }
        return true;
    }

    private boolean validateMessageIdentifier() {
        // Omit leading and trailing whitespace
        return splittedMessage[0].equalsIgnoreCase(ApplicationConstants.TALKER_PROTOCOL_IDENTIFIER);
    }

    public boolean validateInMessageType() {
        switch (splittedMessage[1]) {
            case ApplicationConstants.TALKER_TYPE_REQUEST:
                mHexRequest.isTypeSet = true;
                return true;
            case ApplicationConstants.TALKER_TYPE_RESPONSE:
                mHexResponse.isTypeSet = true;
                return true;
            case ApplicationConstants.TALKER_TYPE_COMMAND:
                // TODO...
                return true;
        }
        return false;
    }

    public boolean validateInMessageCode() {
        if(mHexRequest.isTypeSet) {
            switch (splittedMessage[2]) {
                case ApplicationConstants.TALKER_CODE_LAST_LOCATION:
                    // Last Location request is for Controller
                    // (Optional) Add to Client for additional functionality
                    return true;
            }
        }
        if(mHexResponse.isTypeSet) {
            switch (splittedMessage[2]) {
                case ApplicationConstants.TALKER_CODE_LAST_LOCATION:
                    // RESPONSE --> LAST LOCATION for the client always contains coordinates
                    parseExtra(EXTRA_LOCATION);
                    LatLng latLng = new LatLng(mLatitude, mLongitude);
                    Message msg = mHandler.obtainMessage(
                            MainActivity.HEX_MESSAGE_RESPONSE,
                            CODE_LAST_LOCTATION,
                            -1,
                            latLng);
                    mHandler.sendMessage(msg);
                    mHexResponse.isCodeSet = true;
                    return true;
                case ApplicationConstants.TALKER_CODE_LIVE_LOCATION:
                    // TODO needs error handling
                    parseExtra(EXTRA_TRACKING_STATE);
                    LatLng liveLatLng = new LatLng(mLatitude, mLongitude);
                    Message liveMsg = mHandler.obtainMessage(
                            MainActivity.HEX_MESSAGE_RESPONSE,
                            CODE_LIVE_LOCTATION,
                            -1,
                            liveLatLng);
                    mHandler.sendMessage(liveMsg);
                    mHexResponse.isCodeSet = true;
                    return true;
            }
        }
        return false;
    }

    private void parseExtra(int extraType) {
        // TODO: error handling
        String[] coordinate;
        switch (extraType) {
            case EXTRA_LOCATION:
                MainActivity.logMgr.d(TAG, "extra location extracted: coordinates");
                coordinate = splittedMessage[3].split(ApplicationConstants.TALKER_PARAM_DELIMITER);
                mLatitude = Double.parseDouble(coordinate[0]);
                mLongitude = Double.parseDouble(coordinate[1]);
                MainActivity.logMgr.d(TAG, "lat: " + mLatitude);
                MainActivity.logMgr.d(TAG, "lon: " + mLongitude);
                break;
            case EXTRA_TRACKING_STATE:
                MainActivity.logMgr.d(TAG, "extra location extracted: coordinates");
                coordinate = splittedMessage[3].split(ApplicationConstants.TALKER_PARAM_DELIMITER);
                mLatitude = Double.parseDouble(coordinate[0]);
                mLongitude = Double.parseDouble(coordinate[1]);
                MainActivity.logMgr.d(TAG, "lat: " + mLatitude);
                MainActivity.logMgr.d(TAG, "lon: " + mLongitude);
                break;
            default:
                MainActivity.logMgr.e(TAG, "invalid message extra");
                break;
        }
    }

    public void setInMessage(String msg) {
        if (msg == null)
            MainActivity.logMgr.w(TAG, mContext.getString(R.string.talker_empty_message_warn));
        inMessage = msg;
    }

    public void setType(int type) {
        switch (type) {
            case TYPE_REQUEST:
                messageType = ApplicationConstants.TALKER_TYPE_REQUEST;
                break;
            case TYPE_RESPONSE:
                messageType = ApplicationConstants.TALKER_TYPE_RESPONSE;
                break;
            case TYPE_COMMAND:
                messageType = ApplicationConstants.TALKER_TYPE_COMMAND;
                break;
            default:
                MainActivity.logMgr.e(TAG, "invalid message type");
                break;
        }
    }

    public void setCode(int code) {
        switch (code) {
            case CODE_LAST_LOCTATION:
                messageCode = ApplicationConstants.TALKER_CODE_LAST_LOCATION;
                break;
            case CODE_LIVE_LOCTATION:
                messageCode = ApplicationConstants.TALKER_CODE_LIVE_LOCATION;
                break;
            case CODE_NAV:
                messageCode = ApplicationConstants.TALKER_CODE_NAV;
                break;
            case CODE_AP_ON:
                messageCode = ApplicationConstants.TALKER_CODE_AP_ON;
                break;
            case CODE_AP_OFF:
                messageCode = ApplicationConstants.TALKER_CODE_AP_OFF;
                break;
            default:
                MainActivity.logMgr.e(TAG, "invalid message code");
                break;
        }
    }

    public void setExtra(int extraType, Bundle data) {
        switch (extraType) {
            case EXTRA_LOCATION:
                String lat, lon;
                lat = data.getString(ApplicationConstants.TALKER_BUNDLE_KEY_LATITUDE);
                lon = data.getString(ApplicationConstants.TALKER_BUNDLE_KEY_LONGITUDE);
                formatExtra(lat,lon);
                break;
            case EXTRA_TRACKING_STATE:
                messageExtra = data.getString(ApplicationConstants.TALKER_BUNDLE_KEY_LIVE_STAT);
                break;
            case EXTRA_NAV:
                messageExtra = data.getString(ApplicationConstants.TALKER_BUNDLE_KEY_NAV);
                break;
            case EXTRA_AP_COURSE:
                messageExtra = data.getString(ApplicationConstants.TALKER_BUNDLE_KEY_AP_COURSE);
                break;
            default:
                MainActivity.logMgr.e(TAG, "invalid message extra");
        }
    }

    private void formatExtra(String lat, String lon) {
        messageExtra = lat + ApplicationConstants.TALKER_PARAM_DELIMITER + lon;
    }

    public void formatOutMessage() {
        outMessage.append(messageType);
        outMessage.append(ApplicationConstants.TALKER_DELIMITER);
        outMessage.append(messageCode);
        outMessage.append(ApplicationConstants.TALKER_DELIMITER);
        outMessage.append(messageExtra);

        MainActivity.logMgr.d(TAG, "outMessage = " + outMessage);
    }

    public String getOutMessage() {
        if(outMessage != null)
            return outMessage.toString();
        return mContext.getString(R.string.talker_empty_message_warn);
    }

    public HexRequest getRequest() {
        return this.mHexRequest;
    }

    public HexResponse getResponse() {
        return this.mHexResponse;
    }

    public double getExtraLatitude() {
        return mLatitude;
    }

    public double getExtraLongitude() {
        return mLongitude;
    }

    public void setLatitude(double lat) {
        mLatitude = lat;
    }

    public void setLongitude(double lon) {
        mLongitude = lon;
    }

    // PRIVATE SUB CLASSES
    private class HexRequest {
        private String mCode;
        private String mExtra;
        private boolean isTypeSet;
        private boolean isCodeSet;

        public HexRequest() {
            mCode = "empty";
            mExtra = "empty";
            isTypeSet = false;
            isCodeSet = false;
        }

        private String getCode() {
            return mCode;
        }

        private String getExtra() {
            return mExtra;
        }

        private void setCode(String code) {
            mCode = code;
        }

        private void setExtra(String extra) {
            mExtra = extra;
        }
    }

    private class HexResponse {
        private String mCode;
        private String mExtra;
        private boolean isTypeSet;
        private boolean isCodeSet;


        public HexResponse() {
            mCode = "empty";
            mExtra = "empty";
            isTypeSet = false;
            isCodeSet = false;
        }

        private String getCode() {
            return mCode;
        }

        private String getExtra() {
            return mExtra;
        }

        private void setCode(String code) {
            mCode = code;
        }

        private void setExtra(String extra) {
            mExtra = extra;
        }
    }

}
